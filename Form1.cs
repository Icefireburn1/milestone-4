﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MilestoneProject
{
    public partial class MainForm : Form
    {
        InventoryManager manager;
        
        public MainForm()
        {
            InitializeComponent();
            manager = new InventoryManager();

            dataGridView.DataSource = manager.ItemList; // Set our datasource to our main list
            dataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
        }

        // Add new item to the list
        private void newItemButton_Click(object sender, EventArgs e)
        {
            manager.AddItem(new InventoryItem());
        }

        // Remove item from the list
        private void removeItemButton_Click(object sender, EventArgs e)
        {
            if (manager.ItemList.Count == 0) return;

            manager.RemoveItem(manager.ItemList[dataGridView.CurrentCell.RowIndex]);
        }

        // Change our inventory when the values change
        private void dataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Object cellValue = dataGridView.CurrentCell.Value;
            int listIndex = dataGridView.CurrentCell.RowIndex;

            int intResult = 0;
            float floatResult = 0;
            switch (dataGridView.CurrentCell.OwningColumn.Name)
            {
                case "Name":
                    manager.GetItemByIndex(listIndex).Name = cellValue.ToString();
                    break;

                case "Id":
                    if (Int32.TryParse(cellValue.ToString(), out intResult))
                        manager.GetItemByIndex(listIndex).ID = intResult;
                    else
                        MessageBox.Show("The item ID of the item in row {0} could not be converted to a number. The entry should contain only numbers.", (listIndex + 1).ToString());
                    break;

                case "Cost":
                    if (float.TryParse(cellValue.ToString(), out floatResult))
                        manager.GetItemByIndex(listIndex).Cost = floatResult;
                    else
                        MessageBox.Show("The item Cost of the item in row {0} could not be converted to a number. The entry should contain only numbers and a single decimal", (listIndex + 1).ToString());
                    manager.GetItemByIndex(listIndex).Cost = floatResult;
                    break;

                case "Description":
                    manager.GetItemByIndex(listIndex).Description = cellValue.ToString();
                    break;

                case "onFloor":
                    if (Int32.TryParse(cellValue.ToString(), out intResult))
                        manager.GetItemByIndex(listIndex).OnFloor = intResult;
                    else
                        MessageBox.Show("The item onFloor count of the item in row {0} could not be converted to a number. The entry should contain only numbers.", (listIndex + 1).ToString());
                    break;

                case "capacity":
                    if (Int32.TryParse(cellValue.ToString(), out intResult))
                        manager.GetItemByIndex(listIndex).Capacity = intResult;
                    else
                        MessageBox.Show("The item capacity count of the item in row {0} could not be converted to a number. The entry should contain only numbers.", (listIndex + 1).ToString());
                    break;

                case "Discontinued":
                    if (dataGridView.CurrentCell.Value.ToString() == "True")
                        manager.GetItemByIndex(listIndex).IsDiscontinued = true;
                    else
                        manager.GetItemByIndex(listIndex).IsDiscontinued = false;


                    break;
            }
        }

        // Catch some of the errors that may popup
        private void dataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            string errorMessage = "Error found at row " + (e.RowIndex+1).ToString() + " column " + (e.ColumnIndex+1).ToString() + "\n\n";
            
            // Parsing Error
            if (e.Context.ToString().Contains(DataGridViewDataErrorContexts.Parsing.ToString()))
            {
                errorMessage += "Parsing Error: Cannot convert the currently selected cell to a number. Can this number contain decimals? Does this contain only numbers?\n\n";
                
            }
                
            // Commit Errors
            if (e.Context.ToString().Contains(DataGridViewDataErrorContexts.Commit.ToString()))
            {
                errorMessage += "Commit Error: Could not commit cell change. Was the input appropriate for the entry?\n\n";
            }
                
            // Current Cell Change Error
            if (e.Context.ToString().Contains(DataGridViewDataErrorContexts.CurrentCellChange.ToString()))
            {
                errorMessage += "Cell Change Error: Could not commit cell change. Was the input appropriate for the entry\n\n?";
            }
                
            // Leave Control Error
            if (e.Context.ToString().Contains(DataGridViewDataErrorContexts.LeaveControl.ToString()))
            {
                errorMessage += "Leave Control Error: Changes to the lastly selected cell could not be saved. Was the input valid for the respective field?\n\n";
            }

            dataGridView.RefreshEdit();
            MessageBox.Show(errorMessage);
        }

        // Restock items to make their onFloor count equal their capacity
        private void restockButton_Click(object sender, EventArgs e)
        {
            if (manager.ItemList.Count == 0) return;    // We have no items so skip the rest

            manager.ItemList[dataGridView.CurrentCell.RowIndex].OnFloor = manager.ItemList[dataGridView.CurrentCell.RowIndex].Capacity;
            dataGridView.Refresh();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            List<InventoryItem> tempList = new List<InventoryItem>();
            List<InventoryItem> itemsToBeDeleted = new List<InventoryItem>();   // We can't remove something in the foreach loop of the list we are iterating through
            tempList = manager.ItemList.ToList();
            
            // If Name box not empty
            if (nameInputBox.Text != "")
            {
                // Remove item if it does not have the same name
                foreach (InventoryItem i in tempList)
                {
                    if (i.Name != nameInputBox.Text)
                        itemsToBeDeleted.Add(i);
                }
            }

            // If ID box not empty
            if (IDInputBox.Text != "")
            {
                int result = 0;
                foreach (InventoryItem i in tempList)
                {
                    // Remove item if it does not have the same ID
                    if (Int32.TryParse(IDInputBox.Text, out result))
                    {
                        if (i.ID != result)
                            itemsToBeDeleted.Add(i);
                    }
                    else
                    {
                        MessageBox.Show("Could not convert text in Item Id search box to a number.");
                        IDInputBox.Text = "";
                        break;
                    }
                        
                }
            }
            
            /// Availability Radio Buttons ///////////////////////////////////////
            if (radioCarried.Checked)
            {
                // Remove items that are discontinued
                foreach (InventoryItem i in tempList)
                {
                    if (i.IsDiscontinued)
                        itemsToBeDeleted.Add(i);
                }
            }


            if (radioDiscontinued.Checked)
            {
                // Remove items that are NOT discontinued
                foreach (InventoryItem i in tempList)
                {
                    if (!i.IsDiscontinued)
                        itemsToBeDeleted.Add(i);
                }
            }
            /////////////////////////////////////////////////////////////////////

            /// Stock Radio Buttons ///////////////////////////////////////
            if (radioFullyStocked.Checked)
            {
                // Remove items that the onFloor count does not equal the capacity
                foreach (InventoryItem i in tempList)
                {
                    if (i.OnFloor != i.Capacity || i.OnFloor == 0)
                        itemsToBeDeleted.Add(i);
                }
            }

            if (radioOutOfStock.Checked)
            {
                // Remove items that do not equal 0
                foreach (InventoryItem i in tempList)
                {
                    if (i.OnFloor != 0)
                        itemsToBeDeleted.Add(i);
                }
            }
            /////////////////////////////////////////////////////////////////////

            // Finally, delete all the item in tempList that need to be deleted
            foreach(InventoryItem i in itemsToBeDeleted)
            {
                if (tempList.Contains(i))
                {
                    tempList.Remove(i);
                }
            }

            // Skip the next steps if nothing happened and make sure our datasource is the original one
            if (tempList.SequenceEqual(manager.ItemList))
            {
                dataGridView.DataSource = manager.ItemList;
                return;
            }


            dataGridView.DataSource = tempList;
            dataGridView.Refresh();
        }

        // Exports an inventory text file
        private void exportButton_Click(object sender, EventArgs e)
        {
            StreamWriter outputFile;
            saveDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                // Create file
                outputFile = File.CreateText(saveDialog.FileName);
                outputFile.WriteLine("---Inventory Sheet---");
                // Write to file
                foreach (InventoryItem i in manager.ItemList)
                {
                    outputFile.WriteLine("Name: " + i.Name);
                    outputFile.WriteLine("ID: " + i.ID);
                    outputFile.WriteLine("Cost: " + i.Cost);
                    outputFile.WriteLine("Description: " + i.Description);
                    outputFile.WriteLine("OnFloor: " + i.OnFloor);
                    outputFile.WriteLine("Capacity: " + i.Capacity);
                    outputFile.WriteLine("Discontinued: " + i.IsDiscontinued);
                    outputFile.WriteLine();
                    outputFile.WriteLine();
                }
                outputFile.Close();
            }
            else
                MessageBox.Show("Operation canceled.");
        }

        private void helpButton_Click(object sender, EventArgs e)
        {
            HelpForm hf = new HelpForm();
            hf.ShowDialog();
        }
    }
}
