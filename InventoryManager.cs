﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace MilestoneProject
{
    class InventoryManager
    {
        

        // Return size of itemList array
        public int MaxCount
        {
            get { return itemList.Count; }
        }
        
        // Using BindingList here instead of a standard list since it interacts easier with DataGridView
        private BindingList<InventoryItem> itemList;
        public BindingList<InventoryItem> ItemList
        {
            get { return itemList; }
        }

        public InventoryManager()
        {
            itemList = new BindingList<InventoryItem>();
        }

        // Add item to array list but not if it exists already
        public bool AddItem(InventoryItem item)
        {
            itemList.Add(item);
            return true;
        }

        // Remove an item from the list if it exists
        public bool RemoveItem(InventoryItem item)
        {
            if (itemList.Contains(item))
            {
                itemList.Remove(item);
                return true;
            }
            else
                return false;
                
        }

        // Change an items onFloor count if the item exists
        public bool RestockItem(InventoryItem item, int number)
        {
            foreach(InventoryItem i in itemList)
            {
                if (i == item)
                {
                    i.OnFloor += number;
                    return true;
                }

            }
            return false;
        }

        // Display the items in our list
        // For debug purposes
        public void DisplayItems()
        {
            Console.WriteLine("----Items in the InventoryManager----");
            for (int i = 0; i < itemList.Count; i++)
            {
                if (itemList[i] != null)
                {
                    Console.WriteLine("Name: {0}", itemList[i].Name);
                    Console.WriteLine("ID: {0}", itemList[i].ID);
                    Console.WriteLine("Cost: {0}", itemList[i].Cost);
                    Console.WriteLine("Description: {0}", itemList[i].Description);
                    Console.WriteLine("OnFloor: {0}", itemList[i].OnFloor);
                    Console.WriteLine("Inbackroom: {0}", itemList[i].Capacity);
                    Console.WriteLine("Discontinued: {0}", itemList[i].IsDiscontinued);
                    Console.WriteLine();
                }
            }
            Console.WriteLine("-------------------------------------");
        }

        // Return an item give its name and price
        public InventoryItem SearchForItem(string name, double price)
        {
            foreach(InventoryItem i in itemList)
            {
                if (i.Name == name && i.Cost == price)
                {
                    return i;
                }
                    
            }
            return null;
        }

        // Return the item at the given index
        public InventoryItem GetItemByIndex(int index)
        {
            if (itemList.Count > 0)
                return itemList[index];
            else
                return null;
        }
    }
}
