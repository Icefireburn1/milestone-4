﻿using System;
using System.Collections.Generic;
using System.Text;

// Justin Gewecke

namespace MilestoneProject
{
    class InventoryItem
    {
        // Constructor for when no parameters are given
        public InventoryItem()
        {
            this.name = "Unavailable";
            this.description = "Item has no description";
            this.cost = 0;
            this.id = 0;
            this.onFloor = 0;
            this.capacity = 0;
            this.isDiscontinued = false;
        }
        // Constructor for if parameters are provided
        public InventoryItem(string name, string description, float cost, int id, int onFloor, int capacity, bool isDiscontinued)
        {
            this.name = name;
            this.description = description;
            this.cost = cost;
            this.id = id;
            this.onFloor = onFloor;
            this.capacity = capacity;
            this.isDiscontinued = isDiscontinued;
        }

        public void RestoreToDefault()
        {
            this.name = "Unavailable";
            this.description = "Item has no description";
            this.cost = -1;
            this.id = 0;
            this.onFloor = 0;
            this.capacity = 0;
            this.isDiscontinued = false;
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private float cost;
        public float Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        private int id;
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private int onFloor;
        public int OnFloor
        {
            get { return onFloor; }
            set { onFloor = value; }
        }

        private int capacity;
        public int Capacity
        {
            get { return capacity; }
            set { capacity = value; }
        }

        private bool isDiscontinued;
        public bool IsDiscontinued
        {
            get { return isDiscontinued; }
            set { isDiscontinued = value; }
        }

    }
}
